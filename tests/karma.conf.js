module.exports = function(config){
  config.set({
    basePath : "../",
    jspm: {
        config: "config.js",
        packages: "jspm_packages/",
        loadFiles: ["tests/unit/*.js"],
        serveFiles: ["app/**/*.js"],
    },
    autoWatch : true,
    frameworks: ["jspm", "jasmine"],
    browsers : ["Chrome"],
    plugins : [
        "karma-jspm",
        "karma-chrome-launcher",
        "karma-jasmine"
    ],
  });
}
