import {Printer} from "app/js/printer";

describe("Printer", () => {
    it("2 + 3 = 5", () => {
        let res = 2 + 3;

        expect(res).toEqual(5);
    });

    it("getN should equal 5", () => {
        let printer = new Printer("10");

        let res = printer.getN();

        expect(res).toEqual(5);

    });

    it("5 * 6 should equal 30", () => {
        let printer = new Printer("15"),
            a = 5, b = 6;

        let res = printer.multiply(a, b);

        expect(res).toEqual(30);
    });

    it("-5 * 6 should equal -30", () => {
        let printer = new Printer("15"),
            a = -5, b = 6;

        let res = printer.multiply(a, b);

        expect(res).toEqual(-30);
    });

})
