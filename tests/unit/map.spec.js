import {Map} from "app/js/map";
import * as consts from "app/js/consts";

describe("Map tests", () => {

    it("should create instance of Map", () => {
        let map = new Map(50);

        expect(map).toBeDefined();
        expect(map.blockSize).toEqual(50);
        expect(map.pillSize).toEqual(0);
    });

    it("point should be within the boundaries", () => {
        let point = {x: 10, y: 10},
            map = new Map(50);

        map.reset();
        let res = map.withinBounds(point.y, point.x);

        expect(res).toEqual(true);
    });

    it("point should be on the floor, point - biscuit", () => {
        let point = {x: 4, y: 4},
            map = new Map(50);

        map.reset();
        let res = map.isFloorSpace(point);

        expect(res).toEqual(true);
    });

    it("point should be on the wall", () => {
        let point = {x: 10, y: 0},
            map = new Map(50);

        map.reset();
        let res = map.isWall(point);

        expect(res).toEqual(true);
    });

    it("should reset map using reset method", () => {
        let map = new Map(50);

        map.reset();

        expect(map.height).toEqual(consts.MAP.length);
        expect(map.width).toEqual(consts.MAP[0].length);
        expect(map.map).toEqual(consts.MAP);
    });

    it("get block value, using block method", () => {
        let map = new Map(50),
            point = {x: 4, y: 5};

        map.reset();
        let value = map.block(point);

        expect(value).toEqual(consts.MAP[point.y][point.x]);
    });

    it("set block, using setBlock", () => {
        let map = new Map(50),
            point = {x: 6, y: 4},
            value = consts.BISCUIT;

        map.reset();
        map.setBlock(point, value);
        let res = map.block(point);

        expect(res).toEqual(value);
    });

})
