import {User} from "app/js/pacman";

describe("User", () => {
	
	it("initUser", () => {
        let user = new User();

        user.initUser();

        expect(user.score).toEqual(0);
		expect(user.lives).toEqual(3);
		expect(user.eaten).toEqual(0);
		expect(user.position).toEqual({"x": 90, "y": 120});
    });
	
	it("newLevel", () => {
        let user = new User();

        user.newLevel();

		expect(user.eaten).toEqual(0);
		expect(user.position).toEqual({"x": 90, "y": 120});
    });
	
	it("addScore - +9000", () => {
        let user = new User();
		user.initUser();

        user.addScore(9000);

		expect(user.lives).toEqual(3);
		expect(user.score).toEqual(9000);
    });
	
	it("addScore - +9000  + 9000", () => {
        let user = new User();
		user.initUser();

        user.addScore(9000);

		expect(user.lives).toEqual(3);
		expect(user.score).toEqual(9000);
		
		user.addScore(9000);

		expect(user.lives).toEqual(4);
		expect(user.score).toEqual(18000);	
		
    });
	
	it("loseLife", () => {
        let user = new User();
		user.initUser();

		expect(user.lives).toEqual(3);
		user.loseLife();

		expect(user.lives).toEqual(2);
		
    });

	it("resetPosition", () => {
        let user = new User();
		user.resetPosition();

		expect(user.position).toEqual({"x": 90, "y": 120});
		
    });
	
	it("reset", () => {
        let user = new User();

        user.reset();

        expect(user.score).toEqual(0);
		expect(user.lives).toEqual(3);
		expect(user.eaten).toEqual(0);
		expect(user.position).toEqual({"x": 90, "y": 120});
    });
	
})