import {Ghost} from "app/js/ghost";
import * as consts from "app/js/consts";

describe("Ghost", function() {
	
	it("Null color in constructor, exception should be thrown", function() {
		expect(function () {new Ghost(null);}).toThrow("Color cannot be null");
	});
	
	it("Red color in constructor", function() {
		let ghost = new Ghost("red");
		expect(ghost.getColor()).toBe("red");
	});
	
	it("Eatable is not defined", function() {
		let ghost = new Ghost("red");
		expect(ghost.isVunerable()).toBe(false);
	});
	
	it("Shold be vunarable as eatable was defined", function() {
		let ghost = new Ghost("red");
		ghost.makeEatable();
		expect(ghost.isVunerable()).toBe(true);
	});
	
	it("Should not be dangerous as eatable", function() {
		let ghost = new Ghost("red");
		ghost.makeEatable();
		expect(ghost.isDangerous()).toBe(false);
	});
	
	it("Should be dangerous", function() {
		let ghost = new Ghost("red");
		expect(ghost.isDangerous()).toBe(true);
	});
	
	it("Should not be hidden", function() {
		let ghost = new Ghost("red");
		expect(ghost.isHidden()).toBe(false);
	});
	
	it("Should be hidden as eaten is true", function() {
		let ghost = new Ghost("red");
		ghost.setEaten(true);
		expect(ghost.isHidden()).toBe(true);
	});
	
	it("reset", function() {
		let ghost = new Ghost("red");
		ghost.setEaten(true);
		ghost.makeEatable();
		ghost.reset();
		expect(ghost.isHidden()).toBe(false);
		expect(ghost.isDangerous()).toBe(true);
		expect(ghost.getColor()).toBe("red");
	});
	
	it("eat", function() {
		let ghost = new Ghost("red");
		ghost.eat();
		expect(ghost.isHidden()).toBe(true);
	});
	
	it("oppositeDirection", function() {
		let ghost = new Ghost("red");
		let dir = ghost.oppositeDirection(consts.UP);
		expect(dir === consts.DOWN);
		dir = ghost.oppositeDirection(consts.LEFT);
		expect(dir === consts.RIGHT);
		dir = ghost.oppositeDirection(null);
		expect(dir === null);
	});
	
	it("addBounded", function() {
		let ghost = new Ghost("red");
		let result = ghost.addBounded(3, 3);
		expect(result == 6);
		result = ghost.addBounded(13, 9);
		expect(result == 20);
		result = ghost.addBounded(13, -9);
		expect(result == 19);
	});
	
	it("nextSquare", function() {
		let ghost = new Ghost("red");
		let result = ghost.nextSquare(20, consts.UP);
		expect(result).toBe(20);
		result = ghost.nextSquare(13, consts.RIGHT);
		expect(result).toBe(20);
		result = ghost.nextSquare(13, consts.UP);
		expect(result).toBe(10);
	});
	
	it("getNewCoord", function() {
		let ghost = new Ghost("red");
		let pos = {x: 3, y: 4};
		let result = ghost.getNewCoord(consts.RIGHT, pos);
		expect(result.x).toBe(5);
		result = ghost.getNewCoord(consts.LeFT, pos);
		expect(result.y).toBe(4);
		result = ghost.getNewCoord(consts.UP, pos);
		expect(result.y).toBe(2);
		result = ghost.getNewCoord(consts.DOWN, pos);
		expect(result.y).toBe(6);
	});
	
	it("pane", function() {
		let ghost = new Ghost("red");
		let pos = {x: 3, y: 4};
		expect(ghost.pane(pos)).toBe(false);
		pos = {x: 191, y:100};
		ghost.setDirection(consts.RIGHT);
		let result = ghost.pane(pos);
		expect(result.x).toBe(-10);
		expect(result.y).toBe(100);
		ghost.setDirection(consts.LEFT);
		pos = {x: -10, y: 100};
		result = ghost.pane(pos);
	});
	
	// Test should check only syntax and absense of logical errors like call of methods from other class etc.
	// it("draw", function() {
		// let ghost = new Ghost("red");
		// let game = new module.Game(document.getElementById("viewport"));
		// let ctx = game.mapPainter.context();
		// ghost.draw(ctx);
	// });
});