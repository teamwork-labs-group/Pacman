# Pacman

Репозиторий для лабораторных работ по командной разработке.

Участники: студенты группы ИУ-7-81 Ахтямов, Васюнин, Евтухова.

Вся документация по проекту находится на [вики проекта][wiki].

[wiki]: https://gitlab.com/teamwork-labs-group/Pacman/wikis/home