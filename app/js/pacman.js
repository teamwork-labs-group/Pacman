import * as consts from "./consts";
/**
 * Класс, хранящий информацию о Пакмане(пользователе)
 */
export class User {
	/**
     * Создаёт экземпляр класса User.
     * @param {Game} game - объект, хранящий информацию о текущей игре
     */
	constructor(game) {
		this.position  = null,
        this.direction = null,
        this.eaten     = null,
        this.due       = null, 
        this.lives     = null,
		this.game = game;
        this.score     = 5,
        this.keyMap    = {};
		
		this.keyMap[consts.KEY.ARROW_LEFT]  = consts.LEFT;
		this.keyMap[consts.KEY.ARROW_UP]    = consts.UP;
		this.keyMap[consts.KEY.ARROW_RIGHT] = consts.RIGHT;
		this.keyMap[consts.KEY.ARROW_DOWN]  = consts.DOWN;
    }
	
	/**
     * Инициализирует Пакмана начальными значениями
     */
	initUser() {
        this.score = 0;
        this.lives = 3;
        this.newLevel();
    }   

	newLevel() {
        this.resetPosition();
        this.eaten = 0;
    };
	
	theScore() { 
        return this.score;
    };
	
	getLives() {
        return this.lives;
    };
    
	/**
     * Пересчитывает очки. если их больше 10000, прибавляет 1 жизнь
     * @param {number} nScore - количество очков
     */
    addScore(nScore) { 
		var tmpScore = this.score;
        this.score += nScore;
        if (this.score >= 10000 && tmpScore < 10000) { 
            this.lives += 1;
        }
    };

    loseLife() { 
        this.lives -= 1;
    };

    resetPosition() {
        this.position = {"x": 90, "y": 120};
        this.direction = consts.LEFT;
        this.due = consts.LEFT;
    };
    
    reset() {
        this.initUser();
		this.resetPosition();		
    }; 
	
	/**
     * Отрисовывает Пакмана на карте
     * @param {Context} ctx - контекст рисования на canvas
     */
	draw(ctx) { 

		var s = this.game.map.blockSize, 
		 angle = this.calcAngle(this.direction, this.position),
		 half = s/2;

		ctx.fillStyle = "#FFFF00";

		ctx.beginPath();        

		ctx.moveTo(((this.position.x/10) * s) + half,
				   ((this.position.y/10) * s) + half);
		
		ctx.arc(((this.position.x/10) * s) + half,
				((this.position.y/10) * s) + half,
				s / 2, Math.PI * angle.start, 
				Math.PI * angle.end, angle.direction); 
		
		ctx.fill();    
	};	
	
	/**
     * Отрисовывает Пакмана на карте при проигрыше
     * @param {Context} ctx - контекст рисования на canvas
	 * @param {numbet} amount - угол отрисовки
     */
	drawDead(ctx, amount) { 

        var size = this.game.map.blockSize, 
            half = size / 2;

        if (amount >= 1) { 
            return;
        }

        ctx.fillStyle = "#FFFF00";
        ctx.beginPath();        
        ctx.moveTo(((this.position.x/10) * size) + half, 
                   ((this.position.y/10) * size) + half);
        
        ctx.arc(((this.position.x/10) * size) + half, 
                ((this.position.y/10) * size) + half,
                half, 0, Math.PI * 2 * amount, true); 
        
        ctx.fill();    
    };
	
	calcAngle(dir, pos) { 
        if (dir == consts.RIGHT && (pos.x % 10 < 5)) {
            return {"start":0.25, "end":1.75, "direction": false};
        } else if (dir === consts.DOWN && (pos.y % 10 < 5)) { 
            return {"start":0.75, "end":2.25, "direction": false};
        } else if (dir === consts.UP && (pos.y % 10 < 5)) { 
            return {"start":1.25, "end":1.75, "direction": true};
        } else if (dir === consts.LEFT && (pos.x % 10 < 5)) {             
            return {"start":0.75, "end":1.25, "direction": true};
        }
        return {"start":0, "end":2, "direction": false};
    };
	
	/**
     * Передвижение Пакмана по карте
     * @param {Context} ctx - контекст рисования на canvas
     */
	move(ctx) {
        
        var npos        = null, 
            nextWhole   = null, 
            oldPosition = this.position,
            block       = null;
        
        if (this.due !== this.direction) {
            npos = this.getNewCoord(this.due, this.position);
            
            if (this.isOnSamePlane(this.due, this.direction) || 
                (this.onGridSquare(this.position) && 
                 this.game.map.isFloorSpace(this.next(npos, this.due)))) {
                this.direction = this.due;
            } else {
                npos = null;
            }
        }

        if (npos === null) {
            npos = this.getNewCoord(this.direction, this.position);
        }
        
        if (this.onGridSquare(this.position) && this.game.map.isWallSpace(this.next(npos, this.direction))) {
            this.direction = consts.NONE;
        }

        if (this.direction === consts.NONE) {
            let res = {"new" : this.position, "old" : this.position};
            return  res;
        }
        
        if (npos.y === 100 && npos.x >= 190 && this.direction === consts.RIGHT) {
            npos = {"y": 100, "x": -10};
        }
        
        if (npos.y === 100 && npos.x <= -12 && this.direction === consts.LEFT) {
            npos = {"y": 100, "x": 190};
        }
        
        this.position = npos;        
        nextWhole = this.next(this.position, this.direction);
        
        block = this.game.map.block(nextWhole.y, nextWhole.x);        
        
        if ((this.isMidSquare(this.position.y) || this.isMidSquare(this.position.x)) &&
            block === consts.BISCUIT || block === consts.PILL) {
            
            this.game.map.setBlock(nextWhole, consts.EMPTY);           
            this.addScore((block === consts.BISCUIT) ? 10 : 50);
            this.eaten += 1;
            
            if (this.eaten === 182) {
                this.game.completedLevel();
            }
            
            if (block === consts.PILL) { 
                this.game.eatenPill();
            }
        }   
                
        return {
            "new" : this.position,
            "old" : oldPosition
        };
    };
	
	/**
     * Расчитвает новые координаты
     * @param {numbet} dir - направление движения
	 * @param {Point} current - текущая позиция
     */
	getNewCoord(dir, current) {   
        var newCoord = {
            "x": current.x + (dir === consts.LEFT && -2 || dir === consts.RIGHT && 2 || 0),
            "y": current.y + (dir === consts.DOWN && 2 || dir === consts.UP    && -2 || 0)
        };
        return newCoord;
    };
	
	isOnSamePlane(due, dir) { 
        return ((due === consts.LEFT || due === consts.RIGHT) && 
                (dir === consts.LEFT || dir === consts.RIGHT)) || 
            ((due === consts.UP || due === consts.DOWN) && 
             (dir === consts.UP || dir === consts.DOWN));
    };
	
	onGridSquare(pos) {
        return this.onWholeSquare(pos.y) && this.onWholeSquare(pos.x);
    };
	
	onWholeSquare(x) {
        return x % 10 === 0;
    };
	
	next(pos, dir) {
        return {
            "y" : this.pointToCoord(this.nextSquare(pos.y, dir)),
            "x" : this.pointToCoord(this.nextSquare(pos.x, dir)),
        };                               
    };
	
	pointToCoord(x) {
        return Math.round(x/10);
    };
	
	nextSquare(x, dir) {
        var rem = x % 10;
        if (rem === 0) { 
            return x; 
        } else if (dir === consts.RIGHT || dir === consts.DOWN) { 
            return x + (10 - rem);
        } else {
            return x - rem;
        }
    };
	
	isMidSquare(x) { 
        var rem = x % 10;
        return rem > 3 || rem < 7;
    };
	/**
     * Обрабочик события нажатия клавиши
     * @param {} e - инофрмация о нажатой клавише
     */
	keyDown(e) {
        if (typeof this.keyMap[e.keyCode] !== "undefined") { 
            console.log("keyCode = " + e.keyCode);
            this.due = this.keyMap[e.keyCode];
            console.log("this.due = " + this.due);
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
        return true;
	};
}
