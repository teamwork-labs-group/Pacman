import {DialogPainter, Painter} from "./painter";
import * as consts from "./consts";
import {Map, MapPainter, Point} from "./map";
import {User} from "./pacman";
import {Ghost} from "./ghost";

export 
/**
 * Игра.
 */
class Game extends Painter {

    /**
     * Создает экземпляр класса Game.
     * @param viewport - canvas, где происходит отрисовка игры
     */
    constructor(viewport, blockSize) {
        super(viewport);
        this.blockSize = blockSize;


        this.FPS = 30;
        this.tick = 0;
        this.stateChanged = true;
        this.state = consts.WAITING;
        this.timerStart = 0;
        this.level = 0;
        this.mainFontFamily = "BDCartoonShoutRegular";
        this.mainFontSize = 14;
        this.stored;
        this.level = 0;
        this.delay = 5;


        this.map = new Map(this.blockSize);
        this.mapPainter = new MapPainter(viewport, this.map);
        this.dialog = new DialogPainter(viewport, this.map,
            "#FFFF00", + this.mainFontSize + "px " + this.mainFontFamily
        );

        this.ghosts = [];
        this.ghostSpecs   = ["#00FFDE", "#FF0000", "#FFB8DE", "#FFB847"];
        for (let i = 0, len = this.ghostSpecs.length; i < len; ++i) {
            let ghost = new Ghost(this.ghostSpecs[i],
                this);
            this.ghosts.push(ghost);
        }
		
		this.user = new User(this);
		this.user.initUser();

// может следует перенести в классы, связанные с User?
        this.userPos = new Point();
// может следует перенести в классы, связанные с Ghost?
        this.ghostPos = []; //= new Point();


        this.mapPainter.draw();
        this.dialog.draw({text: "Loading..."});

        this.load();
    }

    get mainFont() {
        var font = this.mainFontSize + "px " + this.mainFontFamily;
        return font;
    }

    get state() {
        return this._state;
    }

    set state(value) {
        this._state = value;
        this.stateChanged = true;
    }

    load() {
        document.addEventListener("keydown", event => {
            var retVal = true;
            var keyCode = event.keyCode;
            if (keyCode === consts.KEY.N)
                this.startNewGame();
            else if (keyCode === consts.KEY.P)
                if (this.state === consts.PAUSE) {
                    this.mapPainter.draw();
                    this.state = this.stored;
                } else {
                    this.stored = this.state;
                    this.state = consts.PAUSE;
                    this.mapPainter.draw();
                    this.dialog.draw({text: "Paused"});
                }
            else if (this.state !== consts.PAUSE) {
                console.log("passed event to user");
                retVal = this.user.keyDown(event);
            }
            return retVal;
        }, true);
        document.addEventListener("keypress", event => {
            if (this.state !== consts.WAITING && this.state !== consts.PAUSE) {
                event.preventDefault();
                event.stopPropagation();
            }
        }, true); 
    }

    startNewGame() {
        this.state = consts.WAITING;
        this.level = 1;
		this.user.reset();
        this.map.reset();
        this.mapPainter.draw();
        this.startLevel();
    }

    startLevel() {
        this.user.resetPosition();
        for (var i = 0; i < this.ghosts.length; i += 1) { 
            this.ghosts[i].reset();
        }
        this.timerStart = this.tick;
        this.state = consts.COUNTDOWN;
    }

    draw(args) {
        var diff, u, i, len, nScore;
        this.ghostPos = [];

        for (i = 0, len = this.ghosts.length; i < len; ++i) {
            this.ghostPos.push(this.ghosts[i].move(this.context));
        }
        u = this.user.move(this.context);
        for (i = 0, len = this.ghosts.length; i < len; ++i) {
            this.mapPainter.redrawBlock(this.ghostPos[i].old);
        }
        this.mapPainter.redrawBlock(u.old);
        for (i = 0, len = this.ghosts.length; i < len; ++i) {
            this.ghosts[i].draw(this.context);
        }
        this.user.draw(this.context);
        this.userPos = u["new"];
        for (i = 0, len = this.ghosts.length; i < len; ++i) {
            if (this.collided(this.userPos, this.ghostPos[i]["new"])) {
                if (this.ghosts[i].isVunerable()) { 
                    this.ghosts[i].eat();
                    eatenCount += 1;
                    nScore = eatenCount * 50;
                    this.drawScore(nScore, this.ghostPos[i]);
                    this.user.addScore(nScore);
                    this.state = consts.EATEN_PAUSE;
                    this.timerStart = this.tick;
                } else if (this.ghosts[i].isDangerous()) {
                    this.state = consts.DYING;
                    this.timerStart = this.tick;
                }
            }
        }
    }

    drawScore(text, position) {
        var blockSize = this.map.blockSize;
        this.context.fillStyle = "#FFFFFF";
        this.context.font = "12px " + this.mainFontFamily;
        this.context.fillText(text,
            position["new"]["x"] / 10 * blockSize,
            (position["new"]["y"] + 5) / 10 * blockSize
        );
    }

    drawFooter() {
        var topLeft = this.map.height*this.map.blockSize;
        var textBase = topLeft + 17;
        var startX = 0.4 * this.map.width * this.map.blockSize;

        this.context.fillStyle = "black";
        this.context.fillRect(0, topLeft, this.map.width*this.map.blockSize, 30);
        this.context.fillStyle = "#FFFF00";
        for (var i = 0, len = this.user.getLives(); i < len; i++) {
            this.context.fillStyle = "#FFFF00";
            this.context.beginPath();
            this.context.moveTo(startX + (25 * i) + this.map.blockSize / 2,
                       (topLeft+1) + this.map.blockSize / 2);
            this.context.arc(startX + (25 * i) + this.map.blockSize / 2,
                    (topLeft+1) + this.map.blockSize / 2,
                    this.map.blockSize / 2, Math.PI * 0.25, Math.PI * 1.75, false);
            this.context.fill();
        }
//
//        ctx.fillStyle = !soundDisabled() ? "#00FF00" : "#FF0000";
//        ctx.font = "bold 16px sans-serif";
//        ctx.fillText("s", 10, textBase);
        this.context.fillStyle = "#FFFF00";
        this.context.font = this.mainFont;
        // нужно получить очки у user и установить сюда
        this.context.fillText("Score: " + this.user.score, 
            0.05 * this.viewport.width, textBase);
        this.context.fillText("Level: " + this.level, 
            0.7 * this.viewport.width, textBase);

    }

    collided(user, ghost) {
        // нужно реализовать ghost и user
        var result = (Math.sqrt(Math.pow(ghost.x - user.x, 2) + 
            Math.pow(ghost.y - user.y, 2))) < 10;
        return result;
    }

    loseLife() {
        this.state = consts.WAITING;
//      нужно реализовать user
        this.user.loseLife();
        if (this.user.getLives() > 0) {
            this.startLevel();
        }
    }

    eatenPill() {
        this.timerStart = this.tick;
        this.eatenCount = 0;
        // нужны призраки
        for (var i = 0; i < this.ghosts.length; i += 1) {
            this.ghosts[i].makeEatable(this.context);
        }
    }
    
    completedLevel() {
        this.state = consts.WAITING;
        this.level += 1;
        this.map.reset();
        this.user.newLevel();
        this.startLevel();
    };

    mainLoop() {
        this.dialog.draw({text: "Press N to Start"});
        this.timer = window.setInterval(() => {
            this._process();
        }, 1000 / this.FPS);
    }

    _process() {

//        console.log(this.tick);

        var diff = 0;
        if (this.state !== consts.PAUSE)
            this.tick++;

        this.mapPainter.drawPills();

        if (this.state === consts.PLAYING)
            this.draw();
        else if (this.state === consts.WAITING && this.stateChanged) {
            this.stateChanged = false;
            this.mapPainter.draw();
            this.dialog.draw({text: "Press N to start a new game"});
        } else if (this.state === consts.EATEN_PAUSE && 
                (this.tick - this.timerStart > this.FPS / 3)) {
            this.mapPainter.draw();
            this.state = consts.PLAYING;
        } else if (this.state === consts.DYING) {
            if (this.tick - this.timerStart > this.FPS * 2) {
                this.loseLife();
            } else {
                this.mapPainter.redrawBlock(this.userPos);
                for (var i = 0, len = this.ghosts.length; i < len; ++i) {
                    this.mapPainter.redrawBlock(this.ghostPos[i].old);
                    this.ghostPos.push(this.ghosts[i].draw(this.context));
                }
                this.user.drawDead(this.context, (this.tick - this.timerStart) / (this.FPS * 2));
            }

        } else if (this.state === consts.COUNTDOWN) {
            diff = this.delay + Math.floor((this.timerStart - this.tick) / this.FPS);
            if (diff === 0) {
                this.mapPainter.draw();
                this.state = consts.PLAYING;
            } else {
                if (diff !== this.lastTime) {
                    this.lastTime = diff;
                    this.mapPainter.draw();
                    this.dialog.draw({text: "Starting in: " + diff});
                }
            }
        }
        this.drawFooter();
    }
}
