export class Painter {
    constructor(viewport) {
        this.viewport = viewport;
    }
    set viewport(value) {
        this._viewport = value;
        this._context = this._viewport.getContext("2d");
    }
    get viewport() {
        return this._viewport;
    }
    get context() {
        return this._context;
    }
    draw(args) {
    }
}

export class DialogPainter extends Painter {
    constructor(viewport, map, fillStyle, font) {
        super(viewport);
        this.map = map;
        this.fillStyle = fillStyle;
        this.font = font;
    }

    draw(args) {
        var text = args.text;
        this.context.fillStyle = this.fillStyle;
        this.context.font = this.font;
        var width = this.context.measureText(text).width,
            x     = ((this.map.width * this.map.blockSize) - width) / 2;
        this.context.fillText(text, x, (this.map.height * 10) + 8);
    }
}
