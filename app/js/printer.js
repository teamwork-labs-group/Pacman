export class Printer {
    constructor(mes) {
        this._mes = mes;
    }

    generateOutput(count) {
        var result = "";
        for (let i = 0; i < count; ++i)
            result += this._mes + " ";
        return result;
    }

    getN() {
        var result = 5;
        return result;
    }

    multiply(a, b) {
        return a * b;
    }
}

