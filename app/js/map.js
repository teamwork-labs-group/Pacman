import * as consts from "./consts";
import {Painter} from "./painter";

export
/**
 * Координата точки на плоскости.
 */
class Point {
    /**
     * Создаёт точку на плоскости.
     * @param {number} x - абсцисса точки
     * @param {number} y - ордината точки
     */
    constructor(x=0, y=0) {
        this.x = x;
        this.y = y;
    }
}

export
/**
 * Размер прямоугольного участка на плоскости.
 */
class Size {
    /**
     * Создаёт экземпляр класса Size.
     * @param {number} width - ширина
     * @param {number} height - высота
     */
    constructor(width=0, height=0) {
        this.width = width;
        this.height = height;
    }
}

export class MapPainter extends Painter {
    constructor(viewport, map) {
        super(viewport);
        this.map = map;
        this.pillSize = 0;
    }

    get blockSize() {
        return this.map.blockSize;
    }

    drawBlock(y, x) {
        var layout = this.map.block(y, x);
        if (layout === consts.PILL)
            return;
        this.context.beginPath();
        if (layout === consts.EMPTY || layout === consts.BLOCK || 
            layout === consts.BISCUIT) {
            this.context.fillStyle = "black";
            this.context.fillRect(x * this.blockSize, y * this.blockSize, 
                this.blockSize, this.blockSize);
            if (layout === consts.BISCUIT) {
                this.context.fillStyle = "white";
                this.context.fillRect(x * this.blockSize + this.blockSize / 2.5,
                    y * this.blockSize + this.blockSize / 2.5, 
                    this.blockSize / 6, this.blockSize / 6);
            }
        }
        this.context.closePath();
    }

    drawWalls() {
        this.context.strokeStyle = "#0000FF";
        this.context.lineWidth = 5;
        this.context.lineCap = "round";

        for (var i = 0; i < this.map.walls.length; ++i) {
            var line = this.map.walls[i];
            this.context.beginPath();
            for (var j = 0; j < line.length; ++j) {
                var p = line[j];
                if (p.move)
                    this.context.moveTo(p.move[0] * this.blockSize, p.move[1] * 
                        this.blockSize);
                else if (p.line)
                    this.context.lineTo(p.line[0] * this.blockSize, p.line[1] * 
                    this.blockSize);
                else if (p.curve)
                    this.context.quadraticCurveTo(p.curve[0] * this.blockSize,
                        p.curve[1] * this.blockSize, p.curve[2] * this.blockSize,
                        p.curve[3] * this.blockSize);
            }
            this.context.stroke();
        }
    }

    draw() {
        var width = this.map.width, height = this.map.height;
        this.context.fillStyle = "black";
        this.context.fillRect(0, 0, width * this.blockSize, height * 
            this.blockSize);
        this.drawWalls();
        for (var i = 0; i < height; ++i)
            for (var j = 0; j < width; ++j)
                this.drawBlock(i, j);
    }

    drawPills() {
        var width = this.map.width, height = this.map.height;
        if (++this.pillSize > 30)
            this.pillSize = 0;
        for (var i = 0; i < height; ++i)
            for (var j = 0; j < width; ++j)
                if (this.map.block(i, j) === consts.PILL) {
                    this.context.beginPath();
                    this.context.fillStyle = "black";
                    this.context.fillRect(j * this.blockSize, i * this.blockSize, 
                        this.blockSize, this.blockSize);
                    this.context.fillStyle = "white";
                    this.context.arc(j * this.blockSize + this.blockSize / 2, 
                        i * this.blockSize + this.blockSize / 2, 
                        Math.abs(5 - (this.pillSize / 3)), 
                        0, Math.PI * 2, false);
                    this.context.fill();
                    this.context.closePath();
                }
    }

    redrawBlock(pos) {
        this.drawBlock(Math.floor(pos.y/10), Math.floor(pos.x/10));
        this.drawBlock(Math.ceil(pos.y/10), Math.ceil(pos.x/10));
    }

}

export
/**
 * Поле, по которому передвигаются пакман и привидения.
 */
class Map {
    _createMap() {
        var mapArr = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 4, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 4, 0],
            [0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0],
            [0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
            [0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
            [2, 2, 2, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 2, 2, 2],
            [0, 0, 0, 0, 1, 0, 1, 0, 0, 3, 0, 0, 1, 0, 1, 0, 0, 0, 0],
            [2, 2, 2, 2, 1, 1, 1, 0, 3, 3, 3, 0, 1, 1, 1, 2, 2, 2, 2],
            [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
            [2, 2, 2, 0, 1, 0, 1, 1, 1, 2, 1, 1, 1, 0, 1, 0, 2, 2, 2],
            [0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
            [0, 4, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 4, 0],
            [0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0],
            [0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
            [0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ];
        return mapArr;
    }

    _createWalls() {
        var walls = [
            [{"move": [0, 9.5]}, {"line": [3, 9.5]},
             {"curve": [3.5, 9.5, 3.5, 9]}, {"line": [3.5, 8]},
             {"curve": [3.5, 7.5, 3, 7.5]}, {"line": [1, 7.5]},
             {"curve": [0.5, 7.5, 0.5, 7]}, {"line": [0.5, 1]},
             {"curve": [0.5, 0.5, 1, 0.5]}, {"line": [9, 0.5]},
             {"curve": [9.5, 0.5, 9.5, 1]}, {"line": [9.5, 3.5]}],

            [{"move": [9.5, 1]},
             {"curve": [9.5, 0.5, 10, 0.5]}, {"line": [18, 0.5]},
             {"curve": [18.5, 0.5, 18.5, 1]}, {"line": [18.5, 7]},
             {"curve": [18.5, 7.5, 18, 7.5]}, {"line": [16, 7.5]},
             {"curve": [15.5, 7.5, 15.5, 8]}, {"line": [15.5, 9]},
             {"curve": [15.5, 9.5, 16, 9.5]}, {"line": [19, 9.5]}],

            [{"move": [2.5, 5.5]}, {"line": [3.5, 5.5]}],

            [{"move": [3, 2.5]},
             {"curve": [3.5, 2.5, 3.5, 3]},
             {"curve": [3.5, 3.5, 3, 3.5]},
             {"curve": [2.5, 3.5, 2.5, 3]},
             {"curve": [2.5, 2.5, 3, 2.5]}],

            [{"move": [15.5, 5.5]}, {"line": [16.5, 5.5]}],

            [{"move": [16, 2.5]}, {"curve": [16.5, 2.5, 16.5, 3]},
             {"curve": [16.5, 3.5, 16, 3.5]}, {"curve": [15.5, 3.5, 15.5, 3]},
             {"curve": [15.5, 2.5, 16, 2.5]}],

            [{"move": [6, 2.5]}, {"line": [7, 2.5]}, {"curve": [7.5, 2.5, 7.5, 3]},
             {"curve": [7.5, 3.5, 7, 3.5]}, {"line": [6, 3.5]},
             {"curve": [5.5, 3.5, 5.5, 3]}, {"curve": [5.5, 2.5, 6, 2.5]}],

            [{"move": [12, 2.5]}, {"line": [13, 2.5]}, {"curve": [13.5, 2.5, 13.5, 3]},
             {"curve": [13.5, 3.5, 13, 3.5]}, {"line": [12, 3.5]},
             {"curve": [11.5, 3.5, 11.5, 3]}, {"curve": [11.5, 2.5, 12, 2.5]}],

            [{"move": [7.5, 5.5]}, {"line": [9, 5.5]}, {"curve": [9.5, 5.5, 9.5, 6]},
             {"line": [9.5, 7.5]}],
            [{"move": [9.5, 6]}, {"curve": [9.5, 5.5, 10.5, 5.5]},
             {"line": [11.5, 5.5]}],


            [{"move": [5.5, 5.5]}, {"line": [5.5, 7]}, {"curve": [5.5, 7.5, 6, 7.5]},
             {"line": [7.5, 7.5]}],
            [{"move": [6, 7.5]}, {"curve": [5.5, 7.5, 5.5, 8]}, {"line": [5.5, 9.5]}],

            [{"move": [13.5, 5.5]}, {"line": [13.5, 7]},
             {"curve": [13.5, 7.5, 13, 7.5]}, {"line": [11.5, 7.5]}],
            [{"move": [13, 7.5]}, {"curve": [13.5, 7.5, 13.5, 8]},
             {"line": [13.5, 9.5]}],

            [{"move": [0, 11.5]}, {"line": [3, 11.5]}, {"curve": [3.5, 11.5, 3.5, 12]},
             {"line": [3.5, 13]}, {"curve": [3.5, 13.5, 3, 13.5]}, {"line": [1, 13.5]},
             {"curve": [0.5, 13.5, 0.5, 14]}, {"line": [0.5, 17]},
             {"curve": [0.5, 17.5, 1, 17.5]}, {"line": [1.5, 17.5]}],
            [{"move": [1, 17.5]}, {"curve": [0.5, 17.5, 0.5, 18]}, {"line": [0.5, 21]},
             {"curve": [0.5, 21.5, 1, 21.5]}, {"line": [18, 21.5]},
             {"curve": [18.5, 21.5, 18.5, 21]}, {"line": [18.5, 18]},
             {"curve": [18.5, 17.5, 18, 17.5]}, {"line": [17.5, 17.5]}],
            [{"move": [18, 17.5]}, {"curve": [18.5, 17.5, 18.5, 17]},
             {"line": [18.5, 14]}, {"curve": [18.5, 13.5, 18, 13.5]},
             {"line": [16, 13.5]}, {"curve": [15.5, 13.5, 15.5, 13]},
             {"line": [15.5, 12]}, {"curve": [15.5, 11.5, 16, 11.5]},
             {"line": [19, 11.5]}],

            [{"move": [5.5, 11.5]}, {"line": [5.5, 13.5]}],
            [{"move": [13.5, 11.5]}, {"line": [13.5, 13.5]}],

            [{"move": [2.5, 15.5]}, {"line": [3, 15.5]},
             {"curve": [3.5, 15.5, 3.5, 16]}, {"line": [3.5, 17.5]}],
            [{"move": [16.5, 15.5]}, {"line": [16, 15.5]},
             {"curve": [15.5, 15.5, 15.5, 16]}, {"line": [15.5, 17.5]}],

            [{"move": [5.5, 15.5]}, {"line": [7.5, 15.5]}],
            [{"move": [11.5, 15.5]}, {"line": [13.5, 15.5]}],
            
            [{"move": [2.5, 19.5]}, {"line": [5, 19.5]},
             {"curve": [5.5, 19.5, 5.5, 19]}, {"line": [5.5, 17.5]}],
            [{"move": [5.5, 19]}, {"curve": [5.5, 19.5, 6, 19.5]},
             {"line": [7.5, 19.5]}],

            [{"move": [11.5, 19.5]}, {"line": [13, 19.5]},
             {"curve": [13.5, 19.5, 13.5, 19]}, {"line": [13.5, 17.5]}],
            [{"move": [13.5, 19]}, {"curve": [13.5, 19.5, 14, 19.5]},
             {"line": [16.5, 19.5]}],

            [{"move": [7.5, 13.5]}, {"line": [9, 13.5]},
             {"curve": [9.5, 13.5, 9.5, 14]}, {"line": [9.5, 15.5]}],
            [{"move": [9.5, 14]}, {"curve": [9.5, 13.5, 10, 13.5]},
             {"line": [11.5, 13.5]}],

            [{"move": [7.5, 17.5]}, {"line": [9, 17.5]},
             {"curve": [9.5, 17.5, 9.5, 18]}, {"line": [9.5, 19.5]}],
            [{"move": [9.5, 18]}, {"curve": [9.5, 17.5, 10, 17.5]},
             {"line": [11.5, 17.5]}],

            [{"move": [8.5, 9.5]}, {"line": [8, 9.5]}, {"curve": [7.5, 9.5, 7.5, 10]},
             {"line": [7.5, 11]}, {"curve": [7.5, 11.5, 8, 11.5]},
             {"line": [11, 11.5]}, {"curve": [11.5, 11.5, 11.5, 11]},
             {"line": [11.5, 10]}, {"curve": [11.5, 9.5, 11, 9.5]},
             {"line": [10.5, 9.5]}]
        ];
        return walls;
    }

    /**
     * Создаёт поле.
     * @param {Size} size - размер поля
     */
    constructor(size) {
        this.height = 0;
        this.width = 0;
        this.blockSize = size;
        this.pillSize = 0;
        this.map;
        this.walls;

        this.reset();
    }

    /**
     * Определяет, находится ли точка внутри поля.
     * @param {number} y - координата y
     * @param {number} x - координата x
     */
    withinBounds(y, x) {
        var result = y >= 0 && y < this.height && x >= 0 && x < this.width;
        return result;
    }

    /**
     * Определяет, находится ли точка внутри поля, вне стен.
     * @param {Point} pos - координата точки на плоскости
     */
    isFloorSpace(pos) {
        var result;
        if (!this.withinBounds(pos.y, pos.x)) {
            result = false;
        } else {
            var piece = this.map[pos.y][pos.x];
            result = piece === consts.EMPTY || 
            piece === consts.BISCUIT ||
            piece === consts.PILL;
        }
        return result;
    }

    /**
     * Определяет, находится ли точка в стене.
     * @param {Point} pos - координата точки на плоскости
     */
    isWall(pos) {
        var result = this.withinBounds(pos.y, pos.x) && 
            this.map[pos.y][pos.x] === consts.WALL;
        return result;
    }

    isWallSpace(pos) {
        return this.isWall(pos);
    }

    /**
     * Очищает поле.
     */
    reset() {
        this.map    = this._createMap();//consts.MAP;//.clone();
        this.walls = this._createWalls();
        this.height = this.map.length;
        this.width  = this.map[0].length;
    }

    /**
     * Получить тип точки по её положению.
     * @param {number} i - x
     * @param {number} j - y
     */
    block(i, j) {
        return this.map[i][j];
    }

    /**
     * Установить тип точки по её координате.
     * @param {Point} pos - координата точки на плоскости
     * @param {number} type - тип точки.
     */
    setBlock(pos, type) {
        this.map[pos.y][pos.x] = type;
    }
}
