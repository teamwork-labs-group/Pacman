import * as consts from "./consts";


export 
/**
 * Привидение
 */
class Ghost {
	/**
     * Создаёт привидение.
     * @param {string} color - цвет привидения
     * @param {Game} game - игра
    */
	constructor(color, game) {
		if (color === null) {
			throw "Color cannot be null";
		}
        this.game = game;
		this.ghost = new GhostEntity(color);
	}

    get map() {
        return this.game.map;
    }

    move(ctx) {
        var oldPos = this.ghost.position,
            onGrid = this.onGridSquare(this.ghost.position),
            npos   = null/*,
            due = this.ghost.due, direction = this.ghost.direction*/;
        
        if (this.ghost.due !== this.ghost.direction) {
            
            npos = this.getNewCoord(this.ghost.due, this.ghost.position);
            
            if (onGrid && this.map.isFloorSpace({
                y: this.pointToCoord(this.nextSquare(npos.y, this.ghost.due)),
                x: this.pointToCoord(this.nextSquare(npos.x, this.ghost.due))
            })) {
                this.ghost.direction = this.ghost.due;
            } else {
                npos = null;
            }
        }
        
        if (npos === null) {
            npos = this.getNewCoord(this.ghost.direction, this.ghost.position);
        }
        
        if (onGrid &&
            this.map.isWallSpace({
                y : this.pointToCoord(this.nextSquare(npos.y, 
                        this.ghost.direction)),
                x : this.pointToCoord(this.nextSquare(npos.x, 
                        this.ghost.direction))
            })) {
            
            this.ghost.due = this.getRandomDirection();
            return this.move(ctx);
        }

        this.ghost.position = npos;        
        
        var tmp = this.pane(this.ghost.position);
        if (tmp) { 
            this.ghost.position = tmp;
        }
        
        this.ghost.due = this.getRandomDirection();
        
        return {
            "new" : this.ghost.position,
            "old" : oldPos
        };
    };

	
	/**
	 * @returns {boolean} Является ли привидение уязвимым для Pacman
	*/
	isVunerable() { 
		return this.ghost.getEatable() !== null;
	};

	/**
	 * @returns {boolean} Является ли привидение опасным для Pacman
	*/
	isDangerous() {
		return this.ghost.getEaten() === null && this.ghost.getEatable() === null;
	};
	
	/**
	 * @returns {boolean} Является ли скрытым
	*/
	isHidden() { 
		return this.ghost.getEatable() === null && this.ghost.getEaten() !== null;
	};

	/**
	 * Установка состояния привидения
	 * @param {boolean} value - признак того, было ли привидение съедено
	*/
	setEaten(value) {
		this.ghost.setEaten(value);
	};
	
	/**
	 * @returns {number} Случайное направление движения привидения
	*/
	getRandomDirection() {
		var direction = this.ghost.getDirection();
        var moves = (direction === consts.LEFT || direction === consts.RIGHT) 
            ? [consts.UP, consts.DOWN] : [consts.LEFT, consts.RIGHT];
        return moves[Math.floor(Math.random() * 2)];
    };
	
	/**
	 * Перевод привидения в режим уязвимости для поедания персонажем Pacman
	*/
	makeEatable() {
		this.ghost.setDirection(this.oppositeDirection(this.ghost.getDirection()));
		this.ghost.setEatable(this.getTick());
	};

	/**
	 * Перевод привидения в режим "съеденный"
	*/
	eat() { 
		this.ghost.setEatable(null);
		this.ghost.setEaten(this.getTick());
		this.ghost.position = {"x": 90, "y": 80};
	};
	
	secondsAgo(tick) { 
        return (this.game.tick - tick) / this.game.FPS;
    };

	/**
	 * @returns {string} Цвет привиденя
	*/
	getColor() { 
		var eatable = this.ghost.getEatable();
		var eaten = this.ghost.getEaten();
		if (eatable) { 
            if (this.secondsAgo(eatable) > 5) { 
                return this.game.tick % 20 > 10 ? "#FFFFFF" : "#0000BB";
            } else { 
                return "#0000BB";
            }
        } else if(eaten) { 
            return "#222";
        } 
		return this.ghost.getColor();
	};
	
	/**
	 * Установка цвета привидения
	 * @param {string} - новый цвет привиденя
	*/
	setColor(color) {
		this.ghost.setColor(color);
	};
	
	// mock for getTick() method
	getTick() {
		return this.game.tick;
	};
	
	/**
	 * Пересоздание привидения с сохранением цвета
	*/
	reset() {
		let color = this.ghost.getColor();
		this.ghost = new GhostEntity(color);
	};
	
	addBounded(x1, x2) { 
        var rem = x1 % 10, 
            result = rem + x2;
        if (rem !== 0 && result > 10) {
            return x1 + (10 - rem);
        } else if(rem > 0 && result < 0) { 
            return x1 - rem;
        }
        return x1 + x2;
    };
	
	/**
	 * Получение новых координат положения привидения
	 * @param {number} dir - направление движения
	 * @param {position} current - текущая позиция привиденя
	 * @returns {position} новая позиция привидения
	*/
	getNewCoord(dir, current) { 
        var speed  = this.isVunerable() ? 1 : this.isHidden() ? 0 : 2,
            xSpeed = (dir === consts.LEFT && -speed || dir === consts.RIGHT && speed || 0),
            ySpeed = (dir === consts.DOWN && speed || dir === consts.UP && -speed || 0);
    
		return {
            "x": this.addBounded(current.x, xSpeed),
            "y": this.addBounded(current.y, ySpeed)
        };
    };
	
	onWholeSquare(x) {
        return x % 10 === 0;
    };
		
	/**
	 * Получение противоположного направления движения
	*/
	oppositeDirection(dir) { 
        return dir === consts.LEFT && consts.RIGHT ||
            dir === consts.RIGHT && consts.LEFT ||
            dir === consts.UP && consts.DOWN || consts.UP;
    };
		
	pointToCoord(x) {
        return Math.round(x / 10);
    };
	
	nextSquare(x, dir) {
        var rem = x % 10;
        if (rem === 0) { 
            return x; 
        } else if (dir === consts.RIGHT || dir === consts.DOWN) { 
            return x + (10 - rem);
        } else {
            return x - rem;
        }
    };
	
	onGridSquare(pos) {
        return this.onWholeSquare(pos.y) && this.onWholeSquare(pos.x);
    };
	
	/** 
	 * Получение позиции при переходе на другой конец поля
	 * @param {position} - текущая позиция привидения
	 * @returns Новая координата положения призрака или false если привидение не находится в области перехода
	*/
	pane(pos) {
        if (pos.y === 100 && pos.x >= 190 && this.ghost.getDirection() === consts.RIGHT) {
            return {"y": 100, "x": -10};
        }
        
        if (pos.y === 100 && pos.x <= -10 && this.ghost.getDirection() === consts.LEFT) {
            return {"y": 100, "x": 190};
        }

        return false;
    };
	
	/**
	 * Установка направления движения 
	 * @param {number} dir - новое направление движения привидения
	*/
	setDirection(dir) {
		this.ghost.setDirection(dir);
	};
	
	/**
	 * Отрисовка привидения
	 * param {context} ctx - context of the application
	*/
	draw(ctx) {
        var direction = this.ghost.getDirection();
        if (!direction)
            return;
        var s    = this.map.blockSize, 
            top  = (this.ghost.position.y/10) * s,
            left = (this.ghost.position.x/10) * s;
    
		let eatable = this.ghost.getEatable();
        if (eatable && this.secondsAgo(eatable) > 8) {
            this.ghost.setEatable(null);
        }
        
		let eaten = this.ghost.getEaten();
        if (eaten && this.secondsAgo(eaten) > 3) { 
            this.ghost.setEaten(null);
        }
        
        var tl = left + s;
        var base = top + s - 3;
        var inc = s / 10;

        var high = this.getTick() % 10 > 5 ? 3  : -3;
        var low  = this.getTick() % 10 > 5 ? -3 : 3;

        ctx.fillStyle = this.getColor();
        ctx.beginPath();

        ctx.moveTo(left, base);

        ctx.quadraticCurveTo(left, top, left + (s/2),  top);
        ctx.quadraticCurveTo(left + s, top, left+s,  base);
        
        // Wavy things at the bottom
        ctx.quadraticCurveTo(tl-(inc*1), base+high, tl - (inc * 2),  base);
        ctx.quadraticCurveTo(tl-(inc*3), base+low, tl - (inc * 4),  base);
        ctx.quadraticCurveTo(tl-(inc*5), base+high, tl - (inc * 6),  base);
        ctx.quadraticCurveTo(tl-(inc*7), base+low, tl - (inc * 8),  base); 
        ctx.quadraticCurveTo(tl-(inc*9), base+high, tl - (inc * 10), base); 

        ctx.closePath();
        ctx.fill();

        ctx.beginPath();
        ctx.fillStyle = "#FFF";
        ctx.arc(left + 6,top + 6, s / 6, 0, 300, false);
        ctx.arc((left + s) - 6,top + 6, s / 6, 0, 300, false);
        ctx.closePath();
        ctx.fill();

        var f = s / 12;
        var off = {};
        off[consts.RIGHT] = [f, 0];
        off[consts.LEFT]  = [-f, 0];
        off[consts.UP]    = [0, -f];
        off[consts.DOWN]  = [0, f];

        ctx.beginPath();
        ctx.fillStyle = "#000";
        //console.log("direction = " + direction);
        ctx.arc(left+6+off[direction][0], top+6+off[direction][1], 
                s / 15, 0, 300, false);
        ctx.arc((left+s)-6+off[direction][0], top+6+off[direction][1], 
                s / 15, 0, 300, false);
        ctx.closePath();
        ctx.fill();
    };
	
	oppositeDirection(dir) { 
        return dir === consts.LEFT && consts.RIGHT ||
            dir === consts.RIGHT && consts.LEFT ||
            dir === consts.UP && consts.DOWN || consts.UP;
    };
	
}

class GhostEntity {

	constructor(color) {
		this.color = color;
		this.eatable = null;
		this.eaten = null;
		this.due = null;
		this.direction = consts.LEFT;
		this.position = {"x": 90, "y": 80};
	}
	
	setEatable(eatable) {
		this.eatable = eatable;
	};
	getEatable() {
		return this.eatable;
	};

	setEaten(eaten) {
		this.eaten = eaten;
	};
	getEaten() {
		return this.eaten;
	};

	setDue(due) {
		this.due = due;
	};
	getDue() {
		return this.due;
	};

	setColor(color) {
		this.color = color;
	}
	getColor() {
		return this.color;
	};
	
	setDirection(dir) {
		this.direction = dir;
	}
	getDirection() {
/*		if (!this.direction)
            this.direction = consts.LEFT;*/
        return this.direction;
	}
	
	setPosition(pos) {
		this.x = pos.x;
		this.y = pos.y;
	}
	getPosition() {
		return {"x": this.x, "y": this.y}
	}
}
